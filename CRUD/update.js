use video;
// pour les tests, on va supprimer deux champs : poster et awards du document The Martian
// on utilisera updateOne() et l'opérateur $unset

db.movieDetails.updateOne(
    // quel document visé
    {
        title: "The Martian"
    },
    // supression
    {
        $unset: {
            poster: ""
        }
    }
);

db.movieDetails.updateOne({
    title: "The Martian"
}, {
    $unset: {
        "awards": ""
    }
})

// updateOne() sur des données scalaires
// opérateur $set
db.movieDetails.updateOne({
    title: "The Martian"
}, {
    $set: {
        poster: "http://ia.media-imdb.com/images/M/MV5BMTc2MTQ3MDA1Nl5BMl5BanBnXkFtZTgwODA3OTI4NjE@._V1_SX300.jpg"
    }
});

db.movieDetails.updateOne({
    title: "The Martian"
}, {
    $set: {
        "awards": {
            "wins": 8,
            "nominations": 14,
            "text": "Nominated for 3 Golden Globes. Another 8 wins & 14 nominations."
        }
    }
});

//opérateur $inc
db.movieDetails.updateOne({
    title: "The Martian"
}, {
    $inc: {
        "tomato.reviews": 3,
        "tomato.userReviews": 25
    }
});

//updateOne sur des champs tableau
db.movieDetails.updateOne({
    title: "The Martian"
}, {
    $push: {
        reviews: {
            rating: 4.5,
            date: ISODate("2016-01-12T09:00:00Z"),
            reviewer: "Spencer H.",
            text: "The Martian could have been a sad drama film, instead it was a hilarious film with a little bit of drama added to it. The Martian is what everybody wants from a space adventure. Ridley Scott can still make great movies and this is one of his best."
        }
    }
})

// utilisation d'un modificateur ($each) associé à un opérateur
// on vérifie que l'on ne possède aucune reviews
db.movieDetails.find({
    title: "The Martian"
}, {
    reviews: 1,
    '_id': 0
}).count();

db.movieDetails.updateOne({
    title: "The Martian"
}, {
    $push: {
        reviews: {
            $each: [{
                rating: 0.5,
                date: ISODate("2016-01-12T07:00:00Z"),
                reviewer: "Yabo A.",
                text: "i believe its ranked high due to its slogan 'Bring him Home' there is nothing in the movie, nothing at all ! Story telling for fiction story !"
            }, {
                rating: 5,
                date: ISODate("2016-01-12T09:00:00Z"),
                reviewer: "Kristina Z.",
                text: "This is a masterpiece. The ending is quite different from the book - the movie provides a resolution whilst a book doesn't."
            }, {
                rating: 2.5,
                date: ISODate("2015-10-26T04:00:00Z"),
                reviewer: "Matthew Samuel",
                text: "There have been better movies made about space, and there are elements of the film that are borderline amateur, such as weak dialogue, an uneven tone, and film cliches."
            }, {
                rating: 4.5,
                date: ISODate("2015-12-13T03:00:00Z"),
                reviewer: "Eugene B",
                text: "This novel-adaptation is humorous, intelligent and captivating in all its visual-grandeur. The Martian highlights an impeccable Matt Damon, power-stacked ensemble and Ridley Scott's masterful direction, which is back in full form."
            }, {
                rating: 4.5,
                date: ISODate("2015-10-22T00:00:00Z"),
                reviewer: "Jens S",
                text: "A declaration of love for the potato, science and the indestructible will to survive. While it clearly is the Matt Damon show (and he is excellent), the supporting cast may be among the strongest seen on film in the last 10 years. An engaging, exciting, funny and beautifully filmed adventure thriller no one should miss."
            }, {
                rating: 4.5,
                date: ISODate("2016-01-12T09:00:00Z"),
                reviewer: "Spencer H.",
                text: "The Martian could have been a sad drama film, instead it was a hilarious film with a little bit of drama added to it. The Martian is what everybody wants from a space adventure. Ridley Scott can still make great movies and this is one of his best."
            }]
        }
    }
});

db.movieDetails.find({
    title: "The Martian"
}, {
    reviews: 1,
    '_id': 0
}).pretty();

// accumulation de modificateurs : $slice: int et $position: int
// on va positionner la nouvelle critique en tête de tableau et
// on ne va conserver que les 5 premiers éléments
db.movieDetails.updateOne({
    title: "The Martian"
}, {
    $push: {
        reviews: {
            $each: [{
                rating: 0.5,
                date: ISODate("2016-01-13T07:00:00Z"),
                reviewer: "Shannon B.",
                text: "Enjoyed watching with my kids!"
            }],
            $position: 0,
            $slice: 5
        }
    }
});


db.movieDetails.find({
        title: "The Martian"
    }, {
        reviews: 1,
        '_id': 0
    }).pretty()
    // vérification numérique
var compte = db.movieDetails.find({
    title: "The Martian"
}, {
    reviews: 1,
    '_id': 0
});
(compte[0].reviews).length;

// updateMany()
// on a des champs avec la valeur null : nous souhaitons les supprimer
db.movieDetails.find({
    rated: null
}).count(); // 1599

/*
Si l'on procède avec $set : on a un problème car on affecte une valeur particulière
à des champs qui ne se conforment pas forcément à cette valeur
(e.g l'Exorciste peut-il être UNRATED ?)
db.movieDetails.updateMany({
    rated: null
}, {
    $set: {
        rated: "UNRATED"
    }
})

Meilleure stratégie :
*/
db.movieDetails.updateMany({
    rated: null
}, {
    $unset: {
        rated: ""
    }
});
// 1599 documents modifiés
db.movieDetails.find({
    rated: null
}).count();
// 1599 documents SANS le champs "rated"

//upserts
// on crée un objet qui est un document :
var newDocument = {
    "title": "The Martian",
    "year": 2015,
    "rated": "PG-13",
    "released": ISODate("2015-10-02T04:00:00Z"),
    "runtime": 144,
    "countries": [
        "USA",
        "UK"
    ],
    "genres": [
        "Adventure",
        "Drama",
        "Sci-Fi"
    ],
    "director": "Ridley Scott",
    "writers": [
        "Drew Goddard",
        "Andy Weir"
    ],
    "actors": [
        "Matt Damon",
        "Jessica Chastain",
        "Kristen Wiig",
        "Jeff Daniels"
    ],
    "plot": "During a manned mission to Mars, Astronaut Mark Watney is presumed dead after a fierce storm and left behind by his crew. But Watney has survived and finds himself stranded and alone on the hostile planet. With only meager supplies, he must draw upon his ingenuity, wit and spirit to subsist and find a way to signal to Earth that he is alive.",
    "poster": "http://ia.media-imdb.com/images/M/MV5BMTc2MTQ3MDA1Nl5BMl5BanBnXkFtZTgwODA3OTI4NjE@._V1_SX300.jpg",
    "imdb": {
        "id": "tt3659388",
        "rating": 8.2,
        "votes": 187881
    },
    "tomato": {
        "meter": 93,
        "image": "certified",
        "rating": 7.9,
        "reviews": 280,
        "fresh": 261,
        "consensus": "Smart, thrilling, and surprisingly funny, The Martian offers a faithful adaptation of the bestselling book that brings out the best in leading man Matt Damon and director Ridley Scott.",
        "userMeter": 92,
        "userRating": 4.3,
        "userReviews": 104999
    },
    "metacritic": 80,
    "awards": {
        "wins": 8,
        "nominations": 14,
        "text": "Nominated for 3 Golden Globes. Another 8 wins & 14 nominations."
    },
    "type": "movie"
};

// si le document existe alors écrase le document stocké dans la base
// sinon tu l'enregistres comme un nouveau document

db.movieDetails.updateOne({
        "imdb.id": newDocument.imdb.id
    }, // cherche un doc dans la base ayant le même imdb.id
    {
        $set: newDocument
    }, // mise à jour
    {
        upsert: true
    } // sinon insère le nouveau document
)

// replaceOne()
db.movies.replaceOne({
        imdb: newDocument.imdb.db
    }, // filtrage : doc à chercher
    {
        newDocument
    } //document de remplacement
);
