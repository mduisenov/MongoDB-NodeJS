// comparaison de supériorité stricte :
//     ">" se traduit par $gt:
//     "<"                $lt:
//     ">="               $gte:
//     "<="               $lte:
//     "=="               $eq:
//     "!="               $ne:
//
//     valeurs se trouvant dans le champs sélectionné : $in: [...]
//     valeurs ne devant pas se trouver dans le champs demandé: $nin: [...]
db.movieDetails.find({
    runtime: {
        $gt: 90
    }
}).count()

// on ajoute une projection
db.movieDetails.find({
    runtime: {
        $gt: 90
    }
}, {
    title: 1,
    runtime: 1,
    _id: 0
}).pretty()

// composition plus élaborée
db.movieDetails.find({
    runtime: {
        $gt: 90,
        $lt: 120
    }
}, {
    title: 1,
    runtime: 1,
    _id: 0
}).count()

// recherche par comparaison dans un document imbriqué
db.movieDetails.find({
    "tomato.meter": {
        $gte: 95
    },
    runtime: {
        $gt: 180
    }
}, {
    title: 1,
    runtime: 1,
    _id: 0
}).pretty()

// exemple d'utilisation de $ne
db.movieDetails.find({
    rated: {
        $ne: "UNRATED"
    }
}).count()

// valeurs devant se trouver dans les documents : $in
db.movieDetails.find({
    rated: {
        $in: ["G", "PG-13"]
    }
}).count()
