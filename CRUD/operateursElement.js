// existence d'un champs dans des documents : $exists
db.movieDetails.find({
    "tomato.meter": {
        $exists: false
    }
}).count()

// vérification du type d'un champs
db.moviesScratch.find({
    "_id": {
        $type: "string"
    }
}).pretty()
