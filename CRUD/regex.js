// recherche toutes les chaines dans le sous-document awards et dans le champs text
// débutant au moins par les trois caractères W o et n
db.movieDetails.find({
    "awards.text": {
        $regex: /^Won.*/
    }
});

// on a rajouté un espace : ainsi le mot Wonder sera refusé
db.movieDetails.find({
    "awards.text": {
        $regex: /^Won\s.*/
    }
});

// on crée une projection
db.movieDetails.find({
    "awards.text": {
        $regex: /^Won\s.*/
    }
}, {
    "title": 1,
    "_id": 0,
    "awards.text": 1
});
