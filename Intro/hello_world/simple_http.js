var http = require('http');

var serveur = http.createServer(
    function(req, rep) {
        rep.writeHead(200, {"Content-Type":"text/plain"});
        rep.end("Hello, world\n");
    }
);
serveur.listen(8000);
console.log("Serveur accessibles sur http://localhost:8000");
