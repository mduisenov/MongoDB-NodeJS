var express = require('express'),
    app= express(),
    moteurs = require('consolidate');

app.engine('html', moteurs.nunjucks);
app.set('view engine', 'html');
app.set('views', __dirname+'/views');

app.get('/', function(requete, reponse){
    reponse.render('hello', {'name':'Templates'});
});

app.use(function(requete, reponse){
    reponse.sendStatus(404);
});

var serveur = app.listen(3000, function(){
    console.log('Serveur Express écoutant sur le port %s',
                serveur.address().port);
});
