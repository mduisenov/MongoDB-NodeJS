// exemple simple d'utilisation d'une insertion multiple avec insertMany()

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

const docs = [
    {"document_numero": 0},
    {"document_numero": 1},
    {"document_numero": 2},
    {"document_numero": 3}
];

const url = "mongodb://localhost:27017/essais";

MongoClient.connect(
    url,
    function(err, db){
        assert.equal(err, null);
        console.log("Connexion réussie avec la base de données");
        //insertion
        db.collection("insertions").insertMany(
            docs,
            function(err, rslt) {
                assert.equal(err, null);
                assert.equal(4, rslt.result.n);
                assert.equal(4, rslt.ops.length)
                console.log("Nombre d'insertions réussies :" + rslt.result.n);
                console.log(rslt.ops);
            }
        );
        db.close();
    }
);
