var cmd = require('command-line-args');

const defintions = [
    {name: "firstYear", alias: "f", type: Number},
    {name: "lastYear", alias: "l", type: Number},
    {name: "employees", alias:"e", type: Number}
];
var options = cmd(defintions);

console.log("JSON généré depuis les éléments passés en paramètre : " + JSON.stringify(options));

if (!(("firstYear" in options) && ("lastYear" in options))) {
    console.log("Erreur : aucun élément fourni. Il faut passer au minimum : -f année et -l année");

}
