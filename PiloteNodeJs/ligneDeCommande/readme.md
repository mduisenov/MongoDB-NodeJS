# Exercices autour de la ligne de commande

## Utilisation des modules :
- command-line-args
- command-line-usage

## Codes :
- **simpleEx.js** : exemple tout simple avec command-line-args seulement.
- **help.js** : exemple utilisant les 2 modules afin de fournir une aide en cas de non présence d'arguments

Pour des exemples plus développés : https://www.npmjs.com/package/command-line-usage
