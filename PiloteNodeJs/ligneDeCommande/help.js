var params = require('command-line-args'),
    usage = require('command-line-usage');

const options = [{
    name: "firstYear",
    alias: "f",
    description: "Année de départ",
    group: "principal",
    type: Number
}, {
    name: "lastYear",
    alias: "l",
    description: "Année de fin",
    group: "principal",
    type: Number
}, {
    name: "employees",
    alias: "e",
    description: "Nombre d'employés recherché au minimum",
    type: Number
}];

const aide = [{
    header: 'Une application typique',
    content: "Génère une aide [italic]{très} importante."
}, {
    header: 'Paramètres',
    optionList: options,
    group: 'principal'
}, {
    header: 'Option',
    optionList: options,
    group: '_none'
}];

if (!(("firstYear" in options) && ("lastYear" in options))) {
    console.log(usage(aide));
} else {
    log(options)
}
