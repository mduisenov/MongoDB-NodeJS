var MongoClient = require('mongodb').MongoClient
assert = require('assert');

// Connexion
MongoClient.connect(
    "mongodb://localhost:27017/crunchbase",
    function(err, db) {
        assert.equal(err, null);
        console.log("Connexion réussie à la base crunchbase");

        var requete = {
            "category_code": "biotech"
        };
        var projection = {
            "name": 1,
            "category_code": 1,
            "_id": 0
        };

        //création du curseur
        var curseur = db.collection('companies')
            .find(requete)
            .project(projection);

        // itération
        curseur.forEach(
            function(doc) {
                console.log(doc.name + " est une compagnie de type " +
                    doc.category_code + "."
                );
                console.log(doc);
            },
            function(err) {
                assert.equal(err, null);
                return db.close();
            }
        );
    }
);
