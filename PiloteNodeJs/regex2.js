// version plus complexe : node regex2 -m "billion.+"
// node regex2 -m "hire.+new"
var MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    cmdLineArgs = require('command-line-args');

var options = lectureCmdArgs();

MongoClient.connect(
    'mongodb://localhost:27017/crunchbase',
    function(err, db) {

        assert.equal(err, null);
        console.log("Connexion réussie à la base de données.");

        var r = requete(options);
        var p = projection(options);

        var curseur = db.collection('companies').find(r, p);

        var compteur = 0

        curseur.forEach(
            function(doc) {
                compteur += 1;
                console.log(doc);
            },
            // curseur vide et erreur
            function(err) {
                // si aucune erreur :
                assert.equal(err, null);
                // alors
                console.log("Notre requête :" + JSON.stringify(r));
                console.log("Nombre de docs retournés : " + compteur);
                return db.close();
            }
        )
    }
);

// lecture des arguments passés en ligne de commande
function lectureCmdArgs() {
    var cli = cmdLineArgs([{
        name: "overview",
        alias: "o",
        type: String
    }, {
        name: "milestones",
        alias: "m",
        type: String
    }]);

    var options = cli.parse();
    // si l'objet "options" est vide
    if (Object.keys(options).length < 1) {
        console.log(cli.getUsage({
            title: "Usage",
            description: "Vous devez spécifier au moins l'une des options présentées ci-dessous"
        }));
        // on quitte
        process.exit();
    }
    return options;
}

// création dynamique de requête
function requete(opt) {
    console.log(opt);
    // création d'un doc JSON vide
    var req = {}

    if ("overview" in opt) {
        req.overview = {
            "$regex": opt.overview,
            // i -> insensible à la casse
            "$options": "i"
        }
    }

    // recherche dans milestones qui est une liste de documents JSON imbriqués
    if ("milestones" in opt) {
        req["milestones.source_description"] = {
            "$regex": opt.milestones,
            "$options": "1"
        }
    }

    return req;
}

// création dynamique de projection
function projection(opt) {
    // l'utilisation d'options ne nous permet plus d'utiliser const
    var p = {
        "_id": 0,
        "name": 1,
        "founded_year": 1,
        "overview": 1
    };

    if ("overview" in opt) {
        p.overview = 1
    }

    if ("milestones" in opt) {
        p["milestones.source_description"] = 1
    }
    return p;
}
