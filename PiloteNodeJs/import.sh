#!/bin/bash
# chmod +x

# on vérifie que le service est bien lancé (test sous Xubuntu 14.04)
sudo service mongod start

# on lance l'importation des documents JSON stockés dans companies.js
mongoimport -d crunchbase -c companies ./Data/companies.js

# on lance le shell Mongo
mongo crunchbase
