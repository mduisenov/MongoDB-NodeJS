var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

MongoClient.connect(
    'mongodb://localhost:27017/crunchbase',
    function(err, db) {
        // on s'attend à ce qu'il n'y ait pas d'erreur à la connexion
        assert.equal(err, null);
        console.log("Connexion effectuée avec la base crunchbase.");

        var requete = {
            "category_code": "biotech"
        };

        // utilisation de .toArray() sur le curseur retourné par find()
        db.collection('companies').find(requete)
            .toArray(
                // docs : tableau contenant l'ensemble des résultats
                function(err, docs) {
                    assert.equal(err, null);
                    // on s'attend à ce que l'ensemble des résultats soit différent de zéro
                    assert.notEqual(docs.length, 0);

                    // itération sur le tableau
                    docs.forEach(
                        function(doc) {
                            console.log(doc.name + " est une compagnie de type " +
                                doc.category_code + ".");
                        }
                    );
                    // fermeture de la connexion
                    db.close();
                    console.log("Nombre total de documents : " + docs.length);
                });
    }
)
