// exemple simple d'utilisation d'insertion unique avec insertOne()

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

const url = "mongodb://localhost:27017/test";

MongoClient.connect(
    url,
    function(err, db) {
        assert.equal(err, null);
        console.log("Connexion réussie à la base de données.");

        db.collection('tests').insertOne(
            // document à insérer
            {
                "document_numero": 1
            },
            // callback
            function(err, rslt) {
                assert.equal(err, null);
                //affichage du document émis par MongoDB :
                //console.log(rslt);
                // affichage du résultat de l'insertion
                console.log(rslt.result);
                console.log(rslt.ops);
                console.log(rslt.insertedCount);
            }
        );

        db.close();
    });
