// utilisation directe du curseur sans recourir à une transformation en un tableau

var MongoClient = require("mongodb").MongoClient,
    assert      = require('assert');

MongoClient.connect(
    'mongodb://localhost:27017/crunchbase',
    function (err, db) {
        // création d'un compteur : nombre de docs itérés
        var compteur = 0;

        assert.equal(err, null);
        console.log("Connexion réussie à la base crunchbase.");

        var requete = {"category_code": "biotech"};
        var curseur = db.collection('companies').find(requete);

        // itération sur le curseur
        curseur.forEach(
            // s'il y a un document
            function(doc) {
                console.log(doc.name + " est une compagnie de type " +
                    doc.category_code + ".");
                compteur += 1;
            },
            // gestion des erreurs et du curseur vidé
            function(err) {
                assert.equal(err, null);
                // curseur est vide : fin de la connexion
                // et affichage de notre compteur
                console.log("Nombre total de docs : " + compteur );
                // on retourne la déclaration de fermeture
                return db.close()
            }
        );
    }
);
